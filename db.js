const Knex = require('knex');

const knex = connect();

function connect () {
  const config = {
    host: 'localhost' || process.env.my_host,
    user: 'root' || process.env.username,
    password: 'root' || process.env.password,
    database: 'simple' || process.env.databasename,
    port:3306  || process.env.db_port
  };

  // Connect to the database
  const knex = Knex({
    client: 'mysql',
    connection: config
  });

  return knex;
}

module.exports = knex;
// npm i  knex --save