const knex = require('../db');


exports.customer = (req,reply) => {
            knex('customer')
            //.where('customer.CUST_CODE', req.params.CUST_CODE)
            .andWhere('customer.CUST_CITY', 'London')
            //.join(' ','','')
            //.select()
            .then(function (row) {
                reply.send({ data: row })
            }).catch(function (error) {
                reply.send({ error: error })
            });
};

exports.agents = (req,reply) => {
        var name = req.query.name;
        knex('agents')
        .where('agents.AGENT_NAME', 'like','%'+name+'%')
        .then((row)=>{
            reply.send({ data: row })
        }).catch((error)=>{
            reply.send({ error: error })
        })
}



exports.test = (req,res) =>{
    var loopdata = [{username:'te',tel:'083xxxxxx'},{username:'oz',tel:'081xxxxxx'},{username:'toe',tel:'083xxxxxx'}]
    res.render('test_loop',{data: loopdata,number :11})
}


exports.inert_customer = (req,reply) => {
    var body = req.body;
    
    var data = {
        "CUST_CODE"     : body.CUST_CODE,
        "CUST_NAME"     : body.CUST_NAME,
        "CUST_CITY"     : body.CUST_CITY,
        "WORKING_AREA"  : body.WORKING_AREA,
        "CUST_COUNTRY"  : body.CUST_COUNTRY,
        "GRADE"         : body.GRADE,
        "OPENING_AMT" : body.OPENING_AMT,
        "RECEIVE_AMT" : body.RECEIVE_AMT,
        "PAYMENT_AMT" : body.PAYMENT_AMT,
        "OUTSTANDING_AMT" : body.OUTSTANDING_AMT,
        "PHONE_NO" : body.PHONE_NO,
        "AGENT_CODE" : body.AGENT_CODE ,
    };
    console.log(data);
    knex('customer')
    .insert(data)
    //.join(' ','','')
    //.select()
    .then((row) =>{
        // reply.send({ data: row })
        reply.redirect("/datatable");
    }).catch( (error) => {
        reply.send({ data: error })
    });
};

exports.list_customer = (req,reply) => {
    var body = req.body;
    

    knex('customer')
    // .get()
    //.join(' ','','')
    //.select()
    .then((row) =>{
        console.log(row);
        reply.render('datatable',{title: 'datatable',row:row});
    }).catch( (error) => {
        reply.send({ data: error })
    });
};

// http://knexjs.org/
exports.update_customer = (req,reply) => {
    var body = req.body;
    
    var data = {
        "CUST_CODE" : body.CUST_CODE,
        "CUST_NAME" : body.CUST_NAME,
        "CUST_CITY" : body.CUST_CITY,
        "PHONE_NO" : body.PHONE_NO,
        "WORKING_AREA" : body.WORKING_AREA,
        "GRADE" : body.GRADE
    };
    console.log(data);
    knex('customer')
    .update(data)
    .where('CUST_CODE',body.id)
    //.join(' ','','')
    //.select()
    .then((row) =>{
        reply.send({ data: row })
    }).catch( (error) => {
        reply.send({ data: error })
    });
};


exports.delete_customer = (req,reply) => {
    var body = req.params;
    knex('customer')
    .where('CUST_CODE',body.id)
    .delete()
    //.join(' ','','')
    //.select()
    .then((row) =>{
        //reply.send({ data: row })
        reply.redirect("/datatable");
    }).catch( (error) => {
        reply.send({ data: error })
    });
};


