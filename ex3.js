const express = require('express')
const app = express()
const port = 3000

app.all('/', function (req, res, next) {
         console.log('Accessing the secret section ...')
        next() // pass control to the next handler
    })
    .get('/', (req, res) => res.send('Hello World!'))
    .post('/', (req, res) => res.send('Hello post!'))
    .put('/', (req, res) => res.send('Hello put!'))
     .delete('/', (req, res) => res.send('Hello delete!'))
// https://expressjs.com/en/4x/api.html#app.METHOD


app.listen(port, () => console.log(`Example app listening on port ${port}!`))