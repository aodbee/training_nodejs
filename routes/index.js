var express = require('express');
var router = express.Router();

const query_db = require('../controller/customer');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/bootsrap', function(req, res, next) {
  res.render('test_bootstrap', { title: 'Express' });
});

router.get('/test', (req, res, next)=>{ 
  res.locals.number= 1000;
  res.render('test',{title: 'Test',name:'Den'})
})
// test variable loop
router.get('/test_loop', (req, res, next)=>{ 
  var loopdata = [{username:'te',tel:'083xxxxxx'},{username:'oz',tel:'081xxxxxx'},{username:'toe',tel:'083xxxxxx'}]
  res.render('test_loop',{data: loopdata,number :10})
})

router.get('/loop_external',query_db.test);


//  test connect sql

router.get('/sql_test',query_db.customer)

router.get('/sql_test_param',query_db.agents)


// npm install --save body-parser multer
router.get('/form_get', (req, res, next)=>{ 
  res.render('form_get',{title: 'form_get'})
})
router.get('/form_get_data', (req, res, next)=>{ 
  res.send({query:req.query});
})

// router.get('/form_get_data',query_db.inert_customer)






//  week 2 
router.get('/datatable',query_db.list_customer);

router.get('/delete_cus/:id',query_db.delete_customer);


router.get('/form_post', (req, res, next)=>{ 
  res.render('form_post',{title: 'form_post'})
})
router.post('/form_post',query_db.inert_customer)
router.post('/form_update_data',query_db.update_customer)
router.post('/data_table',query_db.list_customer)

module.exports = router;
