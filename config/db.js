const Knex = require('knex');

const knex = connect();

function connect () {
  const config = {
    host: 'localhost' || process.env.my_host,
    user: 'root' || process.env.username,
    password: '' || process.env.password,
    database: 'databasename' || process.env.databasename,
    port:3306  || process.env.db_port
  };

  // Connect to the database
  const knex = Knex({
    client: 'mysql',
    connection: config
  });

  return knex;
}

module.exports = knex;
// npm i  knex --save